package com.kcpit;

import java.util.Date;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.kcpit.entities.OrderDelailsEnitiy;
import com.kcpit.repositories.OrderDetailsRepository;

@SpringBootApplication
public class CustomGeneratorsApplication {

	public static void main(String[] args) {
		ApplicationContext context = null;
		OrderDetailsRepository orderRepo = null;
		context = SpringApplication.run(CustomGeneratorsApplication.class, args);
		orderRepo = context.getBean(OrderDetailsRepository.class);
		OrderDelailsEnitiy entity = null;
		entity = new OrderDelailsEnitiy();
		entity.setOrderBy("Zomato");
		entity.setOrderPlaceDate(new Date());
		OrderDelailsEnitiy saveData = orderRepo.save(entity);
		System.out.println(saveData);
	}

}
