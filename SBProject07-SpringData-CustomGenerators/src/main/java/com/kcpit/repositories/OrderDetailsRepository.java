package com.kcpit.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.kcpit.entities.OrderDelailsEnitiy;

public interface OrderDetailsRepository extends JpaRepository<OrderDelailsEnitiy, String> {

}
