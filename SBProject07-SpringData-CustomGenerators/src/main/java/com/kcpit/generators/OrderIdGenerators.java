package com.kcpit.generators;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@PropertySource(value = { "classpath:/src/main/resources/appliaction.properties" })
public class OrderIdGenerators implements IdentifierGenerator {
	@Autowired
	private Environment env;

	@Override
	public Serializable generate(SharedSessionContractImplementor session, Object object) throws HibernateException {
		Connection conn = null;
		Statement st = null;
		ResultSet rs = null;
		Integer seqValue = null;
		String prefixValue = "OD";
		try {
			conn = session.connection();
			st = conn.createStatement();
			rs = st.executeQuery("select order_id_seq.nextval from dual");
			if (rs.next()) {
				seqValue = rs.getInt(1);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} 

		return prefixValue + seqValue;
	}

}
